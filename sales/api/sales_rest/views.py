from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO
from .get_autos import get_autos


# Create your views here.
class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold',
        'id',
    ]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'first_name',
        'last_name',
        "employee_id",
        'id',
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        'first_name',
        'last_name',
        'address',
        'phone_number',
        'id',
    ]


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties =[
        'price',
        'id',
        'automobile',
        'salesperson',
        'customer',
    ]
    encoders = {
        'automobile': AutomobileVODetailEncoder(),
        'salesperson': SalespersonDetailEncoder(),
        'customer': CustomerDetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons":salespersons},
            encoder=SalespersonDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
        salesperson.delete()
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False
        )
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Does not exist"}
        )


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
        customer.delete()
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Does not exist"}
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder
        )
    else:
        # added get autos here to get the most updated info before creating the sale.
        get_autos()
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )

        try:
            employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )

        try:
            phone = content["customer"]
            customer = Customer.objects.get(phone_number=phone)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer phone number"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
        sale.delete()
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Does not exist"}
        )
