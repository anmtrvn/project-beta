from django.db import models

# Create your models here.
# default field for Boolean and unique for vin not required since this is a reference
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.BooleanField()


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(
        max_length=200,
        unique=True,
    )


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.BigIntegerField(unique=True)


class Sale(models.Model):
    price = models.FloatField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE,
    )
