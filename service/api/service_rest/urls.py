from django.urls import path
from .views import technicians_view, appointments_view, appointments_view_cancel, appointments_view_finish

urlpatterns = [
    path("technicians/", technicians_view, name="technicians_view"),
    path("technicians/<int:pk>", technicians_view, name="technicians_view_with_id"),
    path("appointments/", appointments_view, name="appointment_view"),
    path("appointments/<str:vin>", appointments_view, name="appointments_view_with_id"),
    path("appointments/<str:vin>/cancel", appointments_view_cancel, name="appointments_view_with_id_cancel"),
    path("appointments/<str:vin>/finish", appointments_view_finish, name="appointments_view_with_id_finish"),
]
