import { useState } from "react"

function AddSalesperson() {
    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ employeeId, setEmployeeId ] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        };

        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newEmployee = await response.json();
            console.log(newEmployee);
            setEmployeeId('')
            setFirstName('')
            setLastName('')
        }
    }

    function handleFirstNameChange(event) {
        const { value } = event.target;
        setFirstName(value);
    }

    function handleLastNameChange(event) {
        const { value } = event.target;
        setLastName(value);
    }

    function handleEmployeeIdChange(event) {
        const { value } = event.target;
        setEmployeeId(value);
    }

    return(
        <div className="shadow p-4 mt-4">
            <h1>Add Employee</h1>
            <form onSubmit={handleSubmit} id="model-form">
                <div className="form-floating mb-3">
                    <input value={firstName} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={lastName} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={employeeId} onChange={handleEmployeeIdChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                    <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button className="btn btn-primary">Add Employee</button>
            </form>
        </div>
    );
}

export default AddSalesperson
