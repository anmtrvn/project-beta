import './index.css'
import { useState } from 'react';

function AddTechnician() {
    const [newTechnician, setNewTechnician] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    function handleSubmit(event) {
        event.preventDefault();
        const apiURL = "http://localhost:8080/api/technicians/"

        fetch(apiURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newTechnician),
        })
        .then(() => {
            setNewTechnician({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        })
        .catch((error) => console.error('Error creating Technician:', error));
    };

    return (
        <div className="add-technician">
            <h1>Add a Technician</h1>
            <form onSubmit={handleSubmit} className='technician-form'>
                <input
                    className='technician-form-input'
                    type="text"
                    placeholder='First name'
                    value={newTechnician.first_name}
                    onChange={(e) => setNewTechnician(
                        {...newTechnician, first_name:e.target.value})
                    }
                />
                <input
                    className='technician-form-input'
                    type="text"
                    placeholder='Last name'
                    value={newTechnician.last_name}
                    onChange={(e) => setNewTechnician(
                        {...newTechnician, last_name:e.target.value})
                    }
                />
                <input
                    className='technician-form-input'
                    type="text"
                    placeholder='Employee ID'
                    value={newTechnician.employee_id}
                    onChange={(e) => setNewTechnician(
                        {...newTechnician, employee_id:e.target.value})
                    }
                />
                <button
                    className='technician-form-create'
                    type="submit">Create</button>
            </form>
        </div>
    )
}


export default AddTechnician
