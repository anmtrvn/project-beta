import './index.css';
import { useState, useEffect } from 'react';

function ServiceAppointment(props) {
    const [data, setData] = useState([]);
    const [dataVIP, setDataVIP] = useState([]);


    useEffect(() => {
        const apiURL = 'http://localhost:8080/api/appointments/';
        fetch(apiURL)
            .then(response => response.json())
            .then(data => {
                setData(data["appointment"])
            })
            .catch(error => console.error('Error fetching data:', error))
    }, [])

    useEffect(() => {
        const apiURL = 'http://localhost:8100/api/automobiles/';
        fetch(apiURL)
            .then(response => response.json())
            .then(data => setDataVIP(data["autos"]))
            .catch(error => console.error('Error fetching data:', error))
    }, [])

    function handleCancelOrFinish(vin, action) {
        const apiURL = `http://localhost:8080/api/appointments/${vin}/${action}`
        fetch(apiURL,{
            method: 'PUT',
            headers: {
                'Content-type': 'application/json'
            }
        })
        .then(response => {
            if (response.ok) {
                console.log('Appointment canceled or finished successfully!');
                const updatedApiURL = 'http://localhost:8080/api/appointments/';
            fetch(updatedApiURL)
                .then(response => response.json())
                .then(data => {
                setData(data["appointment"]); // Update the local state with the new data
                })
                .catch(error => console.error('Error fetching data:', error));
            } else {
                console.error('Appointment cancellation or finishing failed:', response.status, response.statusText);
            }
          })
          .catch(error => {
            // An error occurred during the fetch, such as a network error or CORS issue
            console.error('Error while making API call:', error);
          });
    }

    const TableRow = ({info}) => {
        let isVIP='No';
        dataVIP.forEach(eachVIP => {
            if(eachVIP.vin === info.vin) {
                isVIP = 'Yes'
            }
        });

        return (
            <>
                <tr>
                    <td>{info.vin}</td>
                    <td>{isVIP}</td>
                    <td>{info.customer}</td>
                    <td>{info.date_time}</td>
                    <td>{`${info.technician.first_name} ${info.technician.last_name}`}</td>
                    <td>{info.reason}</td>
                    {props.history ?
                        <td className='row-btn'>{info.status}</td> :
                        <>
                            <td className='row-btn'><button onClick={() => handleCancelOrFinish(info.vin, 'cancel')}>Cancel</button></td>
                            <td className='row-btn'><button onClick={() => handleCancelOrFinish(info.vin, 'finish')}>Finish</button></td>
                        </>
                    }
                </tr>
            </>
        )
    }
    return(
        <div className='appointment'>
            <h1>Service Apoopintments</h1>
            <table className='appointment-table'>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date and Time</th>
                        <th>Techician</th>
                        <th>Reason</th>
                        {props.history ? <th>Status</th> : null}

                    </tr>

                    {
                        data.map(info => {
                            return (
                                props.history ?
                                <TableRow key={info.id} info={info}/> :
                                info.status !== 'canceled' && info.status !== 'finished' &&
                                <TableRow key={info.id} info={info}/>
                            )
                        })
                    }
            </table>
        </div>
    )

}


export default ServiceAppointment
