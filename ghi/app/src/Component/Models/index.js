import './index.css'
import { useState, useEffect } from 'react';

function Models() {
    const [data, setData] = useState([])

    useEffect(() => {
        const apiURL = 'http://localhost:8100/api/models/';
        fetch(apiURL)
            .then(response => response.json())
            .then(data => setData(data["models"]))
            .catch(error => console.error('Error fetching data:', error))
    }, [])

    const TableRow = ({info}) => {
        return (
            <>
                <tr>
                    <td>{info.name}</td>
                    <td>{info.manufacturer.name}</td>
                    <td><img className='model-image' src={info.picture_url} /></td>
                </tr>
            </>
        )
    }



    return (
        <div className='technicians'>
            <h1>Models</h1>
            <table className='technicians-table'>
                <tbody>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </tbody>


                    {
                        data.map(info => {
                            return (
                                <TableRow key={info.id} info={info}/>
                            )
                        })
                    }
            </table>
        </div>
    )
}

export default Models
