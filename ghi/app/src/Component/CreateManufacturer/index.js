import './index.css'
import { useState } from 'react'

function CreateManufacturer() {
    const [newManufacturer, setNewManufacturer] = useState({
        name: '',
    });

    function handleSubmit(event) {
        event.preventDefault();
        const apiURL = "http://localhost:8100/api/manufacturers/"

        fetch(apiURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newManufacturer),
        })
        .then(() => {
            setNewManufacturer({
                name: '',
            });
        })
        .catch((error) => console.error('Error creating Technician:', error));
    };

    return (
        <div className="create-manufacturer">
            <h1>Create a Manufacturer</h1>
            <form onSubmit={handleSubmit} className='manufacturer-form'>
                <input
                    className='manufacturer-form-input'
                    type="text"
                    placeholder='Manufacturer name'
                    value={newManufacturer.name}
                    onChange={(e) => setNewManufacturer(
                        {...newManufacturer, name:e.target.value})
                    }
                />
                <button
                    className='manufacturer-form-create'
                    type="submit">Create</button>
            </form>
        </div>
    )
}


export default CreateManufacturer
