import { useState } from "react"

function CreateCustomer() {
    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ phoneNumber, setPhoneNumber ] = useState('');
    const [ address, setAddress] = useState('')

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            phone_number: phoneNumber,
            address,
        };

        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);
            setAddress('');
            setFirstName('');
            setLastName('');
            setPhoneNumber('');
        }
    }

    function handleFirstNameChange(event) {
        const { value } = event.target;
        setFirstName(value);
    }

    function handleLastNameChange(event) {
        const { value } = event.target;
        setLastName(value);
    }

    function handleAddressChange(event) {
        const { value } = event.target;
        setAddress(value);
    }

    function handlePhoneNumberChange(event) {
        const{ value } = event.target;
        setPhoneNumber(value)
    }

    return(
        <div className="shadow p-4 mt-4">
            <h1>Create Customer</h1>
            <form onSubmit={handleSubmit} id="model-form">
                <div className="form-floating mb-3">
                    <input value={firstName} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={lastName} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={address} onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={phoneNumber} onChange={handlePhoneNumberChange} placeholder="Phone Number" required type="tel" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create Customer</button>
            </form>
        </div>
    );
}

export default CreateCustomer
