import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useEffect, useState } from 'react';
import Manufacturers from './Manufacturers';
import CreateModel from './CreateModel';
import Automobiles from './Automobiles';
import CreateAuto from './CreateAutomobiles';
import AddSalesperson from './AddSalesperson';
import Salespeople from './Salespeople';
import Customers from './Customers';
import CreateCustomer from './CreateCustomer';
import CreateSale from './CreateSale';
import Sales from './Sales';
import SalesFilter from './SalesFilter';
import AddTechnician from './Component/AddTechnician';
import Technicians from './Component/Technicians';
import CreateServiceAppointment from './Component/CreateServiceAppointment'
import ServiceAppointment from './Component/ServiceAppointment';
import ServiceHistory from './Component/ServiceHistory';
import CreateManufacturer from './Component/CreateManufacturer';
import Models from './Component/Models';

function App() {
  const [ manufacturers, setManufacturers ] = useState ([]);

  async function getManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const { manufacturers } = await response.json();
      setManufacturers(manufacturers);
    } else {
      console.error('Whoops, soemthing happpended')
    }
  }

  useEffect(() => {
    getManufacturers();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<Manufacturers manufacturers={manufacturers} />} />
          </Route>
          <Route path="models">
            <Route path="new" element={<CreateModel manufacturers={manufacturers} />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<Automobiles />} />
            <Route path="new" element={<CreateAuto />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<Salespeople />} />
            <Route path="new" element={<AddSalesperson />} />
          </Route>
          <Route path="customers">
            <Route index element={<Customers />} />
            <Route path="new" element={<CreateCustomer />} />
          </Route>
          <Route path="sales">
            <Route index element={<Sales />} />
            <Route path="new" element={<CreateSale />} />
            <Route path="history" element={<SalesFilter />} />
          </Route>

          {/* service */}
          <Route path="/addTechnician" element={<AddTechnician />} />
          <Route path="/Technician" element={<Technicians />} />
          <Route path="/CreateServiceAppointment" element={<CreateServiceAppointment />} />
          <Route path="/ServiceApppintment" element={<ServiceAppointment />} />
          <Route path="/ServiceHistory" element={<ServiceHistory />} />

          {/* inventory */}
          <Route path="/CreateManufacturer" element={<CreateManufacturer />} />
          <Route path="/ListModels" element={<Models />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
