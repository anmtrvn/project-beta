import { useEffect, useState } from "react";

function SalesFilter() {
    const [sales, setSales] = useState([]);
    const [ salespeople, setSalespersons ] = useState([]);
    const [ salesperson, setSalesperson ] = useState('');
    const [ salespersonSales, setSalespersonSales ] = useState([]);

    async function getSalespersons() {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)

        if (response.ok) {
            const { salespersons } = await response.json();
            setSalespersons(salespersons);
        }
    }

    async function getSales() {
        const url = 'http://localhost:8090/api/sales/';

        const response = await fetch(url);

        if (response.ok) {
          const {sales} = await response.json();
          setSales(sales)
          setSalespersonSales([])
          setSalesperson('')
        }
    }

    useEffect(() => {
        getSales();
        getSalespersons();
    }, [])

    function handleSalespersonChange(event) {
        const { value } = event.target;
        setSalesperson(value);
        const filteredSales = sales.filter(sale => sale.salesperson.employee_id == value);
        setSalespersonSales(filteredSales);
    }

    return(
        <div className="shadow p-4 mt-4">
            <h1>Add a Sale</h1>
            <div className="mb-3">
                <select value={salesperson} onChange={handleSalespersonChange} required name="saleperson" id="saleperson" className="form-select">
                    <option value="">Choose a Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                                </option>
                            );
                        })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersonSales.map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                                <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>${ sale.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesFilter
