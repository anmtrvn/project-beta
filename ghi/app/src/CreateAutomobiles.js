import { useEffect, useState } from "react"

function CreateAuto () {
    const [ models, setModels ] = useState([])
    const [ model, setModel ] = useState('')
    const [ color, setColor ] = useState('')
    const [ year, setYear ] = useState('')
    const [ vin, setVin ] = useState('')



    async function getModels () {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const { models } = await response.json();
            setModels(models);
        }
    }

    useEffect(() => {
        getModels();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            model_id: model,
            color,
            year,
            vin,
        }

        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newAuto = await response.json();
            console.log(newAuto);
            setModel('');
            setColor('');
            setYear('');
            setVin('');
        }
    }

    function handleModelChange(event){
        const { value } = event.target;
        setModel(value);
    }

    function handleColorChange(event) {
        const { value } = event.target;
        setColor(value)
    }

    function handleYearChange(event) {
        const { value } = event.target;
        setYear(value)
    }

    function handleVinChange(event) {
        const { value } = event.target;
        setVin(value)
    }

    return (
        <div className="shadow p-4 mt-4">
            <h1>Create an Auto</h1>
            <form id="auto-form" onSubmit={handleSubmit}>
                <div className="mb-3">
                    <select value={model} onChange={handleModelChange} required name="models" id="models" className="form-select">
                        <option value="">Choose a Model</option>
                        {models.map(model => {
                        return (
                            <option key={model.href} value={model.id}>
                            {model.name}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={year} onChange={handleYearChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" min="1886"/>
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={vin} onChange={handleVinChange} placeholder="VIM" required type="text" name="vin" id="vin" className="form-control" maxLength="17" minLength="17" />
                    <label htmlFor="vin">17-Digit VIN ex:1C3CC5FB2AN120174</label>
                </div>
                <button className="btn btn-primary">Create Automobile</button>
            </form>
        </div>
    )
}

export default CreateAuto
